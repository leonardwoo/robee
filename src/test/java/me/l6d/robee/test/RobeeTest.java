package me.l6d.robee.test;

import me.l6d.robee.gitea.ValidUtil;
import me.l6d.robee.utils.LoggingManager;
import org.junit.jupiter.api.Test;

/**
 * @author Leonard Woo
 */
public class RobeeTest {
  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());

  private static final String PAYLOAD =
      "{\n"
          + "  \"ref\": \"refs/heads/main\",\n"
          + "  \"before\": \"8c69b0e9b4ead5965968aecd1d9f8913d7a7e15d\",\n"
          + "  \"after\": \"8c69b0e9b4ead5965968aecd1d9f8913d7a7e15d\",\n"
          + "  \"compare_url\": \"\",\n"
          + "  \"commits\": [\n"
          + "    {\n"
          + "      \"id\": \"8c69b0e9b4ead5965968aecd1d9f8913d7a7e15d\",\n"
          + "      \"message\": \"init repo\\n\",\n"
          + "      \"url\": \"http://git.app.sigmo.pw/root/test/commit/8c69b0e9b4ead5965968aecd1d9f8913d7a7e15d\",\n"
          + "      \"author\": {\n"
          + "        \"name\": \"Leonard Woo\",\n"
          + "        \"email\": \"leo_916@yahoo.com\",\n"
          + "        \"username\": \"\"\n"
          + "      },\n"
          + "      \"committer\": {\n"
          + "        \"name\": \"Leonard Woo\",\n"
          + "        \"email\": \"leo_916@yahoo.com\",\n"
          + "        \"username\": \"\"\n"
          + "      },\n"
          + "      \"verification\": null,\n"
          + "      \"timestamp\": \"0001-01-01T00:00:00Z\",\n"
          + "      \"added\": null,\n"
          + "      \"removed\": null,\n"
          + "      \"modified\": null\n"
          + "    }\n"
          + "  ],\n"
          + "  \"head_commit\": {\n"
          + "    \"id\": \"8c69b0e9b4ead5965968aecd1d9f8913d7a7e15d\",\n"
          + "    \"message\": \"init repo\\n\",\n"
          + "    \"url\": \"http://git.app.sigmo.pw/root/test/commit/8c69b0e9b4ead5965968aecd1d9f8913d7a7e15d\",\n"
          + "    \"author\": {\n"
          + "      \"name\": \"Leonard Woo\",\n"
          + "      \"email\": \"leo_916@yahoo.com\",\n"
          + "      \"username\": \"\"\n"
          + "    },\n"
          + "    \"committer\": {\n"
          + "      \"name\": \"Leonard Woo\",\n"
          + "      \"email\": \"leo_916@yahoo.com\",\n"
          + "      \"username\": \"\"\n"
          + "    },\n"
          + "    \"verification\": null,\n"
          + "    \"timestamp\": \"0001-01-01T00:00:00Z\",\n"
          + "    \"added\": null,\n"
          + "    \"removed\": null,\n"
          + "    \"modified\": null\n"
          + "  },\n"
          + "  \"repository\": {\n"
          + "    \"id\": 49,\n"
          + "    \"owner\": {\"id\":1,\"login\":\"root\",\"full_name\":\"\",\"email\":\"local@local.host\",\"avatar_url\":\"http://git.app.sigmo.pw/user/avatar/root/-1\",\"language\":\"\",\"is_admin\":false,\"last_login\":\"0001-01-01T00:00:00Z\",\"created\":\"2018-12-28T16:09:48+08:00\",\"restricted\":false,\"active\":false,\"prohibit_login\":false,\"location\":\"\",\"website\":\"\",\"description\":\"\",\"visibility\":\"public\",\"followers_count\":0,\"following_count\":0,\"starred_repos_count\":0,\"username\":\"root\"},\n"
          + "    \"name\": \"test\",\n"
          + "    \"full_name\": \"root/test\",\n"
          + "    \"description\": \"webhook test\",\n"
          + "    \"empty\": false,\n"
          + "    \"private\": false,\n"
          + "    \"fork\": false,\n"
          + "    \"template\": false,\n"
          + "    \"parent\": null,\n"
          + "    \"mirror\": false,\n"
          + "    \"size\": 22,\n"
          + "    \"html_url\": \"http://git.app.sigmo.pw/root/test\",\n"
          + "    \"ssh_url\": \"leo@git.app.sigmo.pw:root/test.git\",\n"
          + "    \"clone_url\": \"http://git.app.sigmo.pw/root/test.git\",\n"
          + "    \"original_url\": \"\",\n"
          + "    \"website\": \"\",\n"
          + "    \"stars_count\": 0,\n"
          + "    \"forks_count\": 0,\n"
          + "    \"watchers_count\": 1,\n"
          + "    \"open_issues_count\": 0,\n"
          + "    \"open_pr_counter\": 0,\n"
          + "    \"release_counter\": 0,\n"
          + "    \"default_branch\": \"main\",\n"
          + "    \"archived\": false,\n"
          + "    \"created_at\": \"2021-10-14T10:00:43+08:00\",\n"
          + "    \"updated_at\": \"2021-10-14T20:33:20+08:00\",\n"
          + "    \"permissions\": {\n"
          + "      \"admin\": false,\n"
          + "      \"push\": false,\n"
          + "      \"pull\": false\n"
          + "    },\n"
          + "    \"has_issues\": true,\n"
          + "    \"internal_tracker\": {\n"
          + "      \"enable_time_tracker\": true,\n"
          + "      \"allow_only_contributors_to_track_time\": true,\n"
          + "      \"enable_issue_dependencies\": true\n"
          + "    },\n"
          + "    \"has_wiki\": false,\n"
          + "    \"has_pull_requests\": false,\n"
          + "    \"has_projects\": false,\n"
          + "    \"ignore_whitespace_conflicts\": false,\n"
          + "    \"allow_merge_commits\": false,\n"
          + "    \"allow_rebase\": false,\n"
          + "    \"allow_rebase_explicit\": false,\n"
          + "    \"allow_squash_merge\": false,\n"
          + "    \"default_merge_style\": \"merge\",\n"
          + "    \"avatar_url\": \"\",\n"
          + "    \"internal\": false,\n"
          + "    \"mirror_interval\": \"\"\n"
          + "  },\n"
          + "  \"pusher\": {\"id\":1,\"login\":\"root\",\"full_name\":\"\",\"email\":\"local@local.host\",\"avatar_url\":\"http://git.app.sigmo.pw/user/avatar/root/-1\",\"language\":\"\",\"is_admin\":false,\"last_login\":\"0001-01-01T00:00:00Z\",\"created\":\"2018-12-28T16:09:48+08:00\",\"restricted\":false,\"active\":false,\"prohibit_login\":false,\"location\":\"\",\"website\":\"\",\"description\":\"\",\"visibility\":\"public\",\"followers_count\":0,\"following_count\":0,\"starred_repos_count\":0,\"username\":\"root\"},\n"
          + "  \"sender\": {\"id\":1,\"login\":\"root\",\"full_name\":\"\",\"email\":\"local@local.host\",\"avatar_url\":\"http://git.app.sigmo.pw/user/avatar/root/-1\",\"language\":\"\",\"is_admin\":false,\"last_login\":\"0001-01-01T00:00:00Z\",\"created\":\"2018-12-28T16:09:48+08:00\",\"restricted\":false,\"active\":false,\"prohibit_login\":false,\"location\":\"\",\"website\":\"\",\"description\":\"\",\"visibility\":\"public\",\"followers_count\":0,\"following_count\":0,\"starred_repos_count\":0,\"username\":\"root\"}\n"
          + "}";

  @Test
  public void test1() {
    ValidUtil valid = new ValidUtil();
    valid.setSecretKey("1234");
    valid.setSignature("69ec904a015496c279501626b11f67214a9494819de2965a38b24acf6f44726c");
    valid.setPayload(PAYLOAD);
    logger.info("" + valid.validate());
  }

}
