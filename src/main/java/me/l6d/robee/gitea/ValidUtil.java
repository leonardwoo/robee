/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.gitea;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.util.Objects;
import me.l6d.robee.http.MediaType;
import me.l6d.robee.utils.LoggingManager;
import org.seppiko.commons.utils.codec.HashCryptUtil;
import org.seppiko.commons.utils.codec.HexUtil;
import org.seppiko.commons.utils.codec.HmacAlgorithms;

/**
 * @author Leonard Woo
 */
public class ValidUtil {

  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());

  public static final String SIGNATURE_HEADER = "X-Gitea-Signature";

  private String secretKey;
  private String signature;
  private String payload;

  public ValidUtil() {
  }

  public ValidUtil(String secretKey, String signature, String payload) {
    this.secretKey = secretKey;
    this.signature = signature;
    this.payload = payload;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public boolean validate() {
    try {
      Objects.requireNonNull(secretKey);
      Objects.requireNonNull(signature);
      Objects.requireNonNull(payload);
      String payloadSignature = HexUtil.encodeToString(
          HashCryptUtil.hmacCrypt(HmacAlgorithms.HMAC_SHA_256, payload.getBytes(StandardCharsets.UTF_8),
              secretKey.getBytes(StandardCharsets.UTF_8)),
          "");
      logger.debug("payload signature: " + payloadSignature);
      return signature.equalsIgnoreCase(payloadSignature);
    } catch (InvalidKeyException | IllegalArgumentException | NullPointerException e) {
      logger.error("", e);
    }
    return false;
  }

  public static boolean validate(String secretKey, String signature, String payload) {
    return new ValidUtil(secretKey, signature, payload).validate();
  }

  public static boolean isJsonType(String mimeType) {
    return MediaType.APPLICATION_JSON.getValue().equals(mimeType.toLowerCase());
  }
}
