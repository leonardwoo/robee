/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.gitea;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;
import me.l6d.robee.configurations.RobeeConfiguration;
import me.l6d.robee.utils.JsonUtil;
import me.l6d.robee.utils.LoggingManager;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;

/**
 * @author Leonard Woo
 */
public class RepositoryUtil {

  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());

  private final RobeeConfiguration configuration = RobeeConfiguration.getInstance();

  private String payload;

  private String branch;

  private String cloneTargetPath;

//  public RepositoryUtil() {
//  }

  public RepositoryUtil(String payload) {
    this.payload = payload;
    this.branch = configuration.getBranch();
    this.cloneTargetPath = configuration.getCloneTarget();
  }

//  public void setPayload(String payload) {
//    this.payload = payload;
//  }

//  public void setBranch(String branch) {
//    this.branch = branch;
//  }

//  public void setCloneTargetPath(String cloneTargetPath) {
//    this.cloneTargetPath = cloneTargetPath;
//  }

  public boolean cloneRepo() {
    try {
      // parser Gitea webhook
      String cloneUrl = JsonUtil.fromJson(payload).getAsJsonObject()
          .get("repository").getAsJsonObject()
          .get("clone_url").getAsString();
      cloneUrl = Objects.requireNonNull(cloneUrl);

      // if directory is not empty, clean it
      if (!cleanDirectory(cloneTargetPath)) {
        return false;
      }

      Git.cloneRepository()
          .setURI(cloneUrl)
          .setBranch(branch)
          .setDirectory(Paths.get(cloneTargetPath).toFile())
          .call();
    } catch (Throwable cause) {
      logger.error("", cause);
      return false;
    }
    return true;
  }

  private boolean cleanDirectory(String pathname) {
    try {
      FileUtils.cleanDirectory(Paths.get(pathname).toFile());
    } catch (IOException ex) {
      return false;
    }
    return true;
  }

}
