/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee;

import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.ServletInfo;
import java.util.ArrayList;
import me.l6d.robee.actions.IndexAction;
import me.l6d.robee.actions.WebhookAction;
import me.l6d.robee.configurations.RobeeConfiguration;
import me.l6d.robee.utils.LoggingManager;
import me.l6d.robee.utils.UndertowUtil;

/**
 * @author Leonard Woo
 */
public class RobeeApplication {

  public static void main(String[] args) {
    LoggingManager logger = LoggingManager.getInstance().getLogger("robee");
    try {
      RobeeConfiguration config = RobeeConfiguration.getInstance();
      ArrayList<ServletInfo> servlets = new ArrayList<>();
      servlets.add(Servlets.servlet("index", IndexAction.class).addMapping("/"));
      servlets.add(Servlets.servlet("webhook", WebhookAction.class).addMapping("/webhook"));

      UndertowUtil.start(config.getBind(), servlets.toArray(ServletInfo[]::new));
    } catch (Throwable t) {
      logger.error("", t);
    }
  }
}
