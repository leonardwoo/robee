/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.utils;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ServletInfo;
import java.net.InetSocketAddress;
import me.l6d.robee.actions.ErrAction;
import org.xnio.Options;

/**
 * @author Leonard Woo
 */
public class UndertowUtil {

  public static final String ROOT_PATH = "/";
  public static final String DEPLOYMENT_NAME = "robee.war";

  public static void start(InetSocketAddress bind, ServletInfo[] actions) throws Throwable {
    DeploymentInfo servletBuilder = Servlets.deployment()
        .setClassLoader(UndertowUtil.class.getClassLoader())
        .setContextPath(ROOT_PATH)
        .setDeploymentName(DEPLOYMENT_NAME)
//        .addFilters()
        .addServlets(actions)
        .setExceptionHandler(new ErrAction().get());

    DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
    manager.deploy();

    PathHandler path = Handlers.path()
        .addPrefixPath(ROOT_PATH, manager.start());

    Undertow server =
        Undertow.builder()
            .addHttpListener(bind.getPort(), bind.getHostName())
            .setHandler(path)
            .setWorkerOption(Options.KEEP_ALIVE, false)
            .build();

    server.start();
  }

}
