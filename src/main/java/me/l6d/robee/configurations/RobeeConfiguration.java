/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.configurations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import me.l6d.robee.utils.JsonUtil;
import me.l6d.robee.utils.LoggingManager;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.ObjectUtil;
import org.seppiko.commons.utils.StreamUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * @author Leonard Woo
 */
public class RobeeConfiguration {

  private static RobeeConfiguration instance = new RobeeConfiguration();
  public static RobeeConfiguration getInstance() {
    return instance;
  }

  private RobeeConfiguration(){
    init();
  }

  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());

  private void init() {
    try {
      InputStream is = StreamUtil.getStream(this.getClass(),
          System.getProperty("robee." + Environment.CONFIG_FILE_PARAMETER_SUFFIX,
              Environment.CONFIG_FILENAME_JSON) );
      if (ObjectUtil.isNull(is)) {
        is = StreamUtil.getStream(Environment.CONFIG_FILENAME_JSON);
      }
      if(ObjectUtil.isNull(is)) {
        throw new FileNotFoundException("Configuration file not found");
      }

      JsonElement root = JsonUtil.fromJson( StreamUtil.loadReader(is) );
      logger.info("Config: " + root.toString());
      loadConfig(root.getAsJsonObject());

    } catch (Throwable t) {
      logger.error("Configuration initialization failed", t);
    }

  }

  private void loadConfig(JsonObject root) {
    // config file structure
    String server = root.get("server").getAsString();
    if (StringUtil.hasLength(server) && server.contains(":")) {
      String[] _server = server.split("\\:");
      bindHost = _server[0];
      bindPort = Integer.parseInt(_server[1]);
    }

    this.branch = root.get("branch").getAsString();
    this.cloneTarget = root.get("cloneTarget").getAsString();

    this.secretKey = root.get("gitea").getAsJsonObject().get("secretKey").getAsString();
  }

  private String bindHost;
  private int bindPort;

  public InetSocketAddress getBind() {
    return new InetSocketAddress(bindHost, bindPort);
  }

  private String branch;

  public String getBranch() {
    return branch;
  }

  private String cloneTarget;

  public String getCloneTarget() {
    if (StringUtil.isEmpty(this.cloneTarget)) {
      this.cloneTarget = "./robee/";
    }
    return cloneTarget;
  }

  private String secretKey;

  public String getSecretKey() {
    return secretKey;
  }
}
