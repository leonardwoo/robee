/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.http;

/**
 * @author Leonard Woo
 */
public enum MediaType {

  ALL("*/*"),

  APPLICATION_RSS_XML("application/rss+xml"),
  APPLICATION_ATOM_XML("application/atom+xml"),
  APPLICATION_XML("application/xml"),
  APPLICATION_XML_DTD("application/xml-dtd"),

  APPLICATION_STREAM_JSON("application/stream+json"),
  APPLICATION_JSON("application/json"),
  APPLICATION_OCTET_STREAM("application/octet-stream"),

  APPLICATION_FORM_URLENCODED("application/x-www-form-urlencoded"),
  MULTIPART_FORM_DATA("multipart/form-data"),

  TEXT_CSS("text/css"),
  APPLICATION_JAVASCRIPT("application/javascript"),
  TEXT_JAVASCRIPT("text/javascript"),
  TEXT_HTML("text/html"),

  TEXT_PLAIN("text/plain"),
  TEXT_MARKDOWN("text/markdown"),
  TEXT_ASCIIDOC("text/asciidoc"),

  TEXT_CSV("text/csv"),
  TEXT_XML("text/xml"),

  TEXT_EVENT_STREAM("text/event-stream");

  private final String value;
  MediaType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

}
