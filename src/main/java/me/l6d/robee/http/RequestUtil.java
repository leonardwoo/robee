/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.http;

import java.io.BufferedReader;
import java.io.IOException;
import org.seppiko.commons.utils.Environment;

/**
 * @author Leonard Woo
 */
public class RequestUtil {

  public static String readerToString(BufferedReader reader, boolean removeWhitespace, boolean ignoreNewLine) throws IOException {
    StringBuilder sb = new StringBuilder();
    while(reader.ready()) {
      String line = reader.readLine();
      if (removeWhitespace) {
        line = line.strip();
      }

      sb.append(line);
      if (!ignoreNewLine) {
        sb.append(Environment.NEW_LINE);
      }
    }
    return sb.toString();
  }

  public static String readerToString(BufferedReader reader) throws IOException {
    StringBuilder sb = new StringBuilder();
    while (reader.ready()) {
      sb.append((char) reader.read());
    }
    return sb.toString();
  }
}
