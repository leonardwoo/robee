/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.actions;

import io.undertow.server.HttpServerExchange;
import io.undertow.servlet.api.ExceptionHandler;
import io.undertow.util.StatusCodes;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import me.l6d.robee.http.HttpStatus;
import me.l6d.robee.http.MediaType;
import me.l6d.robee.models.ResponseMessageEntity;
import me.l6d.robee.utils.JsonUtil;
import me.l6d.robee.utils.LoggingManager;

/**
 * @author Leonard Woo
 */
public class ErrAction implements ExceptionHandler {

  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());

  public ExceptionHandler get() {
    return this;
  }

  private String service() {
    return new ResponseMessageEntity(HttpStatus.INTERNAL_SERVER_ERROR.getCode(),
        "SERVER ERROR!!!").toString();
  }

  @Override
  public boolean handleThrowable(HttpServerExchange httpServerExchange,
      ServletRequest servletRequest, ServletResponse servletResponse, Throwable throwable) {
    try {
      httpServerExchange.setStatusCode(StatusCodes.INTERNAL_SERVER_ERROR);
      servletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
      servletResponse.setContentType(MediaType.APPLICATION_JSON.getValue());
      byte[] body = service().getBytes(StandardCharsets.UTF_8);
      servletResponse.setContentLength(body.length);
      OutputStream os = servletResponse.getOutputStream();
      os.write(body);
      os.flush();
      os.close();

      logger.warn("WARNING: SERVER ERROR!!!", throwable);
    } catch (IOException e) {
      logger.error("Failed to return exception message.", e);
      return false;
    }
    return true;
  }
}
