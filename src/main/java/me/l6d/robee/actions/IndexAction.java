/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import me.l6d.robee.http.HttpStatus;
import me.l6d.robee.http.MediaType;
import me.l6d.robee.http.ResponseEntity;
import me.l6d.robee.models.ResponseMessageEntity;
import me.l6d.robee.utils.LoggingManager;
import org.seppiko.commons.utils.http.HttpMethod;

/**
 * @author Leonard Woo
 */
public class IndexAction extends AbstractAction {

//  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());

  protected ResponseEntity ContentHandleExecution(HttpServletRequest req) throws ServletException, IOException {
//    if (!req.getMethod().equalsIgnoreCase(HttpMethod.POST.name())) {
//      return methodNotAllowed();
//    }

//    req.getHeaderNames().asIterator()
//        .forEachRemaining(
//            e -> {
//              String headerValue = req.getHeader(e);
//              logger.info(e + ": " + headerValue);
//            });
//    logger.info("----------------");
//
//    BufferedReader body = req.getReader();
//    while (body.ready()) {
//      logger.info(body.readLine());
//    }
//    logger.info("-----------------");

    return sendJson(HttpStatus.OK, new ResponseMessageEntity(400, "Bad request"));
  }
}
