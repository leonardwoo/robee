/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.actions;

import io.undertow.Version;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.l6d.robee.http.HttpStatus;
import me.l6d.robee.http.MediaType;
import me.l6d.robee.http.ResponseEntity;
import me.l6d.robee.models.ResponseMessageEntity;
import org.seppiko.commons.utils.ObjectUtil;

/**
 * @author Leonard Woo
 */
public abstract class AbstractAction extends HttpServlet {

  protected abstract ResponseEntity ContentHandleExecution(HttpServletRequest request) throws ServletException, IOException;

  protected ResponseEntity methodNotAllowed() {
    String json = new ResponseMessageEntity(405, "Method Not Allowed.").toString();
    return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED, MediaType.APPLICATION_JSON, json.getBytes(StandardCharsets.UTF_8));
  }

  protected ResponseEntity badRequest(String msg) {
    String json = new ResponseMessageEntity(400, msg).toString();
    return new ResponseEntity(HttpStatus.BAD_REQUEST, MediaType.APPLICATION_JSON, json.getBytes(StandardCharsets.UTF_8));
  }

  protected ResponseEntity sendJson(HttpStatus status, ResponseMessageEntity respMsg) {
    return new ResponseEntity(status, MediaType.APPLICATION_JSON, respMsg.toString().getBytes(StandardCharsets.UTF_8));
  }

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    ResponseEntity chxResp = ContentHandleExecution(req);
    if (ObjectUtil.isNull(chxResp)) {
      throw new ServletException("Action method is null");
    }
    resp.setStatus(chxResp.getStatusCode());
    resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
    resp.addHeader("server", "Undertow/" + Version.getVersionString());
    resp.setContentType(chxResp.getTypeValue());
    byte[] body = chxResp.getBody();
    resp.setContentLength(body.length);
    OutputStream os = resp.getOutputStream();
    os.write(body);
    os.flush();
    os.close();
  }
}
