/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.robee.actions;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import me.l6d.robee.configurations.RobeeConfiguration;
import me.l6d.robee.gitea.RepositoryUtil;
import me.l6d.robee.gitea.ValidUtil;
import me.l6d.robee.http.HttpStatus;
import me.l6d.robee.http.MediaType;
import me.l6d.robee.http.RequestUtil;
import me.l6d.robee.http.ResponseEntity;
import me.l6d.robee.models.ResponseMessageEntity;
import me.l6d.robee.utils.LoggingManager;
import org.seppiko.commons.utils.http.HttpMethod;

/**
 * @author Leonard Woo
 */
public class WebhookAction extends AbstractAction {

  private final LoggingManager logger = LoggingManager.getInstance().getLogger(this.getClass());
  private final RobeeConfiguration configuration = RobeeConfiguration.getInstance();

  @Override
  protected ResponseEntity ContentHandleExecution(HttpServletRequest request)
      throws ServletException, IOException {
    if (!request.getMethod().equalsIgnoreCase(HttpMethod.POST.name())) {
      return methodNotAllowed();
    }

    if (!ValidUtil.isJsonType(request.getContentType())) {
      return badRequest("Wrong content-type.");
    }

    var body = RequestUtil.readerToString(request.getReader());
    if (!ValidUtil.validate(configuration.getSecretKey(), request.getHeader(ValidUtil.SIGNATURE_HEADER), body)) {
      return badRequest("Signature verification failed.");
    }

    RepositoryUtil repo = new RepositoryUtil(body);
    if (!repo.cloneRepo()) {
      return sendJson(HttpStatus.OK, new ResponseMessageEntity(400, "Git clone failed"));
    }

    return sendJson(HttpStatus.OK, new ResponseMessageEntity(200, "Successfully"));
  }
}
